package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * sorts based on classes names.
 * @author Javier
 */
public class SSortClassesByName extends SCommand {

  @Override
  public void executeOn(SProject project) {
    List<SPackage> temp = project.packages();
    for (SPackage pack:temp) {
      List<SClass> clase = pack.classes();
      for (SClass sc : clase) {
        sc.changeHierarchy(0);
      }
      Collections.sort(clase,new Comparator<SClass>() {
        public int compare(SClass sc1, SClass sc2) {
          return sc1.className().compareTo(sc2.className());
        }
      });
    }

  }

}
