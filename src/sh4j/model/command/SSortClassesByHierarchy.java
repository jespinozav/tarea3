package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.ArrayList;
import java.util.List;

/**
 * sorts based on hierarchy class.
 * @author Javier
 *
 */
public class SSortClassesByHierarchy extends SCommand {

  @Override
  public void executeOn(SProject project) {
    SSortClassesByName cls = new SSortClassesByName();
    cls.executeOn(project);
    List<SPackage> list = project.packages();
    for (SPackage temp : list) {
      if (temp.toString().equals("\\sh4j\\parser") || temp.toString().equals("\\sh4j\\ui")) {
        continue;
      } else {
        List<SClass> classpkg = temp.classes();
        List<SClass> sorts = sortc("Object",classpkg,0);
        classpkg.clear();
        classpkg.addAll(sorts);
      }
    }
  }
  /**
   * sorts the lists based on hierarchy.
   * @param father class
   * @param cls list
   * @param jerarquia number
   * @return arranged list
   */
  public List<SClass> sortc(String father, List<SClass> cls, int jerarquia) {
    List<SClass> aux = new ArrayList<SClass>();
    for (SClass cd: cls) {
      if (cd.superClass().equals(father)) {
        cd.changeHierarchy(jerarquia);
        aux.add(cd);
        int jer = jerarquia+1;
        aux.addAll(sortc(cd.className(),cls, jer));
      }
    }
    return aux;
  }
}
