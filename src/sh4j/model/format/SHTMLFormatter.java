package sh4j.model.format;


import sh4j.model.highlight.SDummy;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;
import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;


/**
 * class html formatter for browser.
 * @author Javier
 *
 */
public class SHTMLFormatter implements SFormatter {
  /**
   * string buffer html.
   */
  private final StringBuffer buffer;
  /**
   * level variable.
   */
  private int level;
  /**
   * sstyle object.
   */
  private final SStyle style;
  /**
   * highlighters of style.
   */
  private final SHighlighter[] highlighters;
  /**
   * number of lines of code.
   */
  private int numLineas;
  
  /**
   * boolean of number lines.
   */
  private boolean num;
  
  /**
   * constructor of class.
   * @param style sstyle object.
   * @param hs highlighters.
   */
  public SHTMLFormatter(SStyle style, SHighlighter... hs) {
    this.style = style;
    highlighters = hs;
    buffer = new StringBuffer();
    numLineas = 1;
    num = true;
  }
  /**
   * look up words.
   * @param text text to be highlighted
   * @return h highlighter
   */
  private SHighlighter lookup(String text) {
    for (SHighlighter h : highlighters) {
      if (h.needsHighLight(text)) {
        return h;
      }
    }
    return new SDummy();
  }
  
  @Override
  public boolean numeration() {
    return num;
  }
  
  @Override
  public void styledWord(String word) {
    buffer.append(lookup(word).highlight(word, style));
  }

  @Override
  public void styledChar(char cas) {
    buffer.append(lookup(cas + "").highlight(cas + "", style));
  }

  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  @Override
  public void styledCR() {
    if (style.nume().bool()) {
      buffer.append("\n");
      buffer.append(tag("span", "  " + Integer.toString(numLineas) + " ","background:#f1f0f0;"));
      ++numLineas;
      indent();
    } else {
      numLineas = 1;
      buffer.append("\n");
      indent();
    }
  }

  @Override
  public void styledBlock(SBlock bas) {
    level++;
    for (SText text : bas.texts()) {
      text.export(this);
    }
    level--;
  }
  /**
   * indent htmlformatter.
   */
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }
  }

  @Override
  public String formattedText() {
    return style.formatBody(buffer.toString());
  }

  public static String tag(String name, String content, String style) {
    return "<" + name + " style='" + style + "'>" + content + "</" + name + ">";
  }

}
