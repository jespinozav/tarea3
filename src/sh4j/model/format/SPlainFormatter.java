package sh4j.model.format;

import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;
/**
 * plain formatter class.
 * @author Javier
 */
public class SPlainFormatter implements SFormatter {
  /**
   * string buffer.
   */
  private final StringBuffer buffer;
  /**
   * int level.
   */
  private int level;
  /**
   * constructor of class.
   */
  public SPlainFormatter() {
    buffer = new StringBuffer();
  }

  @Override
  public void styledWord(String word) {
    buffer.append(word);
  }

  @Override
  public void styledChar(char cas) {
    buffer.append(cas);
  }

  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  @Override
  public void styledCR() {
    buffer.append('\n');
    indent();
  }

  @Override
  public void styledBlock(SBlock bas) {
    level++;
    for (SText text : bas.texts()) {
      text.export(this);
    }
    level--;
  }
  /**
   * indent plainformatter.
   */
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }
  }

  @Override
  public String formattedText() {
    return buffer.toString();
  }

  @Override
  public boolean numeration() {
    return false;
  }

}
