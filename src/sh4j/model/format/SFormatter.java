package sh4j.model.format;

import sh4j.parser.model.SBlock;
/**
 * sformatter class.
 * @author Javier
 *
 */
public interface SFormatter {
  /**
   * styled word.
   * @param word string
   */
  public void styledWord(String word);
  /**
   * styled char.
   * @param c char
   */
  public void styledChar(char cas);
  /**
   * styled space.
   */
  public void styledSpace();
  /**
   * styled enter.
   */
  public void styledCR();
  /**
   * styled block.
   * @param b sblock object
   */
  public void styledBlock(SBlock bas);
  /**
   * formatted text.
   * @return text formatted
   */
  public String formattedText();
  
  /**
   * return boolean of num.
   * @return boolean num
   */
  public boolean numeration();
}
