package sh4j.model.style;

import sh4j.parser.model.SBoolean;

/**
 * sstyle class.
 * @author Javier
 *
 */
public interface SStyle {
  /**
   * text style.
   * @return text style
   */
  public String toString();
  /**
   * class highlighted.
   * @param text to highlight
   * @return highlighted text
   */
  public String formatClassName(String text);
  /**
   * curly bracket highlighted.
   * @param text to highlight
   * @return highlighted text
   */
  public String formatCurlyBracket(String text);
  /**
   * keyword highlighted.
   * @param text to highlight
   * @return highlighted text
   */
  public String formatKeyWord(String text);
  /**
   * pseudovariable highlighted.
   * @param text to highlight
   * @return highlighted text
   */
  public String formatPseudoVariable(String text);
  /**
   * semicolon highlighted.
   * @param text to highlight
   * @return highlighted text
   */
  public String formatSemiColon(String text);
  /**
   * string highlighted.
   * @param text to highlight
   * @return highlighted text
   */
  public String formatString(String text);
  /**
   * mainclass highlighted.
   * @param text to highlight
   * @return highlighted text
   */
  public String formatMainClass(String text);
  /**
   * modifier highlighted.
   * @param text to highlight
   * @return highlighted text
   */
  public String formatModifier(String text);
  /**
   * body highlighted.
   * @param text to highlight
   * @return highlighted text
   */
  public String formatBody(String text);
  /**
   * returns sboolean.
   * @return sboolean variable
   */
  public SBoolean nume();
}
