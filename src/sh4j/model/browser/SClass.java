package sh4j.model.browser;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

/**
 * class that represents class.
 * @author Javier
 *
 */
public class SClass implements SObject {
  /**
   * declaration paramether.
   */
  private final TypeDeclaration declaration;
  /**
   * list of methods object.
   */
  private final List<SMethod> methods;
  /**
   * hierarchy number.
   */
  private int jerarquia;
  /**
   * constructor.
   * @param td type declaration
   * @param ms list method
   */
  public SClass(TypeDeclaration td, List<SMethod> ms) {
    declaration = td;
    methods = ms;
    jerarquia = 0;
  }
  /**
   * method list.
   * @return list of methods
   */
  public List<SMethod> methods() {
    return methods;
  }
  /**
   * class name.
   * @return class name 
   */
  public String className() {
    return declaration.getName().getIdentifier();
  }
  /**
   * is interface.
   * @return boolean if it's interface
   */
  public boolean isInterface() {
    return declaration.isInterface();
  }
  /**
   * super class.
   * @return super class
   */
  public String superClass() {
    if (declaration.getSuperclassType() == null) {
      return "Object";
    }
    return declaration.getSuperclassType().toString();
  }
  /**
   * change hierarchy.
   * @param i number
   */
  public void changeHierarchy(int i) {
    jerarquia = i;
  }
  /**
   * return hierarchy variable.
   * @return hierarchy variable
   */
  public int getHierarchy() {
    return jerarquia;
  }
  
  /**
   * class name.
   * @return class name
   */
  public String toString() {
    return className();
  }
  
  @Override
  public Font font() {
    if (this.isInterface()) {
      return new Font("Helvetica", Font.ITALIC, 12);
    }
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  @Override
  public String icon() {
    if (this.isInterface()) {
      return "./resources/int_obj.gif"; 
    }
    return "./resources/class_obj.gif";
  }

  @Override
  public int getNumLines() {
    int num = 0;
    for (SMethod m : methods) {
      num += m.getLinesOfCode();
    }
    return num;
  }
  @Override
  public int getNumClasses() {
    return 1;
  }
  @Override
  public int getYellowColor() {
    int yellow = 0;
    for(SMethod m : methods) {
      yellow += m.getYellowColor(); 
    }
    return yellow;
  }
  @Override
  public Color background() {
    return null;
  }

}
