package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * sproject class.
 * @author Javier
 *
 */
public class SProject implements SObject {
  /**
   * list of packages.
   */
  private final List<SPackage> packages;

  /**
   * constructor of class.
   */
  public SProject() {
    packages = new ArrayList<SPackage>();  
  }
  /**
   * add a package to project.
   * @param pack package to be added
   */
  public void addPackage(SPackage pack) {
    packages.add(pack);
  }
  

  /**
   * return the project packages.
   * @return packages paramether
   */
  public List<SPackage> packages() {
    return packages;
  }
  /**
   * gets a certain package.
   * @param pkgName package to be desired
   * @return package desired
   */
  public SPackage get(String pkgName) {
    for (SPackage pkg : packages) {
      if (pkg.toString().equals(pkgName)) {
        return pkg;
      }
    }
    return null;
  }
  
  /**
   * get number of classes in the entire project.
   * @return number of classes.
   */
    
  @Override
  public Font font() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String icon() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Color background() {
    // TODO Auto-generated method stub
    return null;
  }
  
  @Override
  public int getNumLines() {
    int total = 0;
    for (SPackage temp : packages) {
      total += temp.getNumLines();
    }
    return total;
  }
  
  @Override
  public int getNumClasses() {
    int total = 0;
    for (SPackage temp : packages) {
      total += temp.getNumClasses();
    }
    return total;
  }
  
  @Override
  public int getYellowColor() {
    int yellow = 0;
    for (SPackage temp : packages) {
      yellow += temp.getYellowColor();
    }
    return yellow;
  }
  @Override
  public int getHierarchy() {
    // TODO Auto-generated method stub
    return 0;
  }

}
