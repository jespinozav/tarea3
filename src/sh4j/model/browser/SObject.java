package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;

/**
 * interface that will be implemented for the various browsers.
 * @author Javier
 */

public interface SObject {
  
  /**
   * returns a font based on class.
   * @return a Font object
   */
  public Font font();

  /**
   * returns an icon based on the object.
   * @return icon based on object
   */
  public String icon();

  /**
   * returns color based on number of lines.
   * @return a Color object
   */
  public Color background();
  
  /**
   * return number of lines.
   * @return num of code
   */
  public int getNumLines();
  
  /**
   * get num of classes.
   * @return number of classes
   */
  public int getNumClasses();
  
  /**
   * get number of yellow codes.
   * @return number of yellow codes
   */
  public int getYellowColor();
  /**
   * gets hierarchy number.
   * @return hierarchy variable
   */
  public int getHierarchy();
  
}
