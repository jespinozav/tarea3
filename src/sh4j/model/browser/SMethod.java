package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import sh4j.parser.model.SBlock;

import java.awt.Color;
import java.awt.Font;


/**
 * class that corresponds to the methods.
 * @author Javier
 *
 */
public class SMethod implements SObject {
  /**
   * declaration paramether.
   */
  private final MethodDeclaration declaration;
  /**
   * sblock body that allows for counting lines.
   */
  private final SBlock body;
  /**
   * number of lines of code.
   */
  private final int numlinas;

  /**
   * constructor of the class.
   * @param node method declaration object
   * @param body sblock object 
   */
  public SMethod(MethodDeclaration node, SBlock body) {
    declaration = node;
    this.body = body;
    numlinas = body.getLines();
  }
  /**
   * modifier of method.
   * @return modifier keyword
   */
  public String modifier() {
    for (Object obj : declaration.modifiers()) {
      if (obj instanceof Modifier) {
        Modifier modifier = (Modifier) obj;
        return modifier.getKeyword().toString();
      }
    }
    return "default";
  }
  /**
   * identifier of name.
   * @return name of declaration
   */
  public String name() {
    return declaration.getName().getIdentifier();
  }
  /**
   * body of method.
   * @return body of method
   */
  public SBlock body() {
    return body;
  }
  /**
   * returns name.
   * @return name to string
   */
  public String toString() {
    return name();
  }

  @Override
  public String icon() {
    if (this.modifier().equals("public")) {
      return "./resources/public_co.gif";
    } else if (this.modifier().equals("protected")) {
      return "./resources/protected_co.gif";
    } else if (this.modifier().equals("private")) {
      return "./resources/private_co.gif";
    } else {
      return "./resources/default_co.gif";
    }
  }

  @Override
  public Color background() {
    if (numlinas < 30) {
      return Color.WHITE;
    } else if (numlinas > 30 && numlinas < 50) { 
      return Color.YELLOW;
    } else {
      return Color.RED;
    }
  }

  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }
  
  public int getLinesOfCode() {
    return numlinas;
  }
  
  @Override
  public int getNumLines() {
    return getLinesOfCode();
  }
  
  @Override
  public int getNumClasses() {
    return 0;
  }
  
  @Override
  public int getYellowColor() {
    if (numlinas > 30) {
      return 1;
    }
    return 0;
  }
  @Override
  public int getHierarchy() {
    // TODO Auto-generated method stub
    return 0;
  }
}
