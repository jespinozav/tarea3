package sh4j.parser.model;
/**
 * boolean class to set numeration.
 * @author Javier
 *
 */
public class SBoolean {
  /**
   * boolean valor.
   */
  private boolean valor;
  /**
   * constructor of class.
   */
  public SBoolean() {
    valor = true;
  }
  
  /**
   * returns boolean valor.
   * @return valor variable
   */
  public boolean bool() {
    return valor;
  }
  
  /**
   * change valor of boolean.
   */
  public void change() {
    if (valor) {
      valor = false;
    } else {
      valor = true;
    }
  }
}
