package sh4j.ui;

import java.awt.Color;
import java.awt.Component;


import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SObject;

/**
 * Each object of this class represent a cell in the browser for instance, package cell, class cell
 * and method cell.
 * 
 * @author juampi
 *
 */
@SuppressWarnings("serial")
public class SItemRenderer extends JLabel implements ListCellRenderer<SObject> {
  protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

  /**
   * create a item renderer.
   */
  public SItemRenderer() {
    setOpaque(true);
    setBorder(BorderFactory.createLineBorder(Color.WHITE));
  }

  /**
   * return a JLabel for each SObject in the JList.
   */
  public Component getListCellRendererComponent(JList<? extends SObject> list, SObject value,
      int index, boolean isSelected, boolean cellHasFocus) {
    StringBuffer buffer = new StringBuffer();
    for (int i = 0; i < value.getHierarchy(); i++) {
      buffer.append("   ");
    }
    buffer.append("");
    String b = buffer.toString();
    setText(b + value.toString());
    setFont(value.font());
    setIcon(new ImageIcon(value.icon()));
    setBackground(value.background());
    if (isSelected) {
      setBorder(BorderFactory.createLineBorder(Color.BLUE));
    } else {
      setBorder(BorderFactory.createLineBorder(Color.WHITE));
    }
    return this;
  }


}
