package sh4j.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SFactory;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.model.command.SCommand;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SEclipseStyle;
import sh4j.model.style.SStyle;

/**
 * JBrowser
 * 
 * @author juampi
 *
 */
@SuppressWarnings({"unchecked", "serial"})
public class SFrame extends JFrame {
  /**
   * Shighlighters objects.
   */
  private SHighlighter[] lighters;
  /**
   * style to be applied.
   */
  protected SStyle style = new SEclipseStyle();
  /**
   * jlist of packages.
   */
  private JList<SPackage> packages;
  /**
   * jlist of classes.
   */
  private JList<SClass> classes;
  /**
   * jlist of methods.
   */
  private JList<SMethod> methods;
  /**
   * editor for html.
   */
  private JEditorPane htmlPanel;
  /**
   * menu for files.
   */
  private JMenu file;
  /**
   * menu for styles.
   */
  private JMenu estilos;
  /**
   * menu for numeration.
   */
  private JMenu numeration;
  /**
   * project of sframe.
   */
  private SProject project;
  /**
   * status bar of project.
   */
  private JPanel statusbar;
  /**
   * number of lines.
   */
  private int numLineas;
  /**
   * number of classes.
   */
  private int numClases;
  /**
   * number of yellows.
   */
  private int numYellows;

  /**
   * Create a browser, it will use the style and the lighters passed in the argutments.
   * 
   * @param style
   * @param lighters
   */
  public SFrame(SStyle style, SHighlighter... lighters) {
    super("CC3002 Browser");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);
    setSize(450, 750);
    statusbar = new JPanel();
    build();
    this.lighters = lighters;
    this.style = style;
    JMenuBar bar = new JMenuBar();
    file = new JMenu("Sort");
    estilos = new JMenu("Styles");
    numeration = new JMenu("Numeration");
    bar.add(file);
    bar.add(estilos);
    bar.add(numeration);
    setJMenuBar(bar);
  }
  /**
   * add numerations.
   * @param str list of strings
   */
  public void addNums(String... str) {
    for (String m : str) {
      addNum(m);
    }
  }
  /**
   * add numeration.
   * @param m string of numeration
   */
  public void addNum(final String m) {
    JMenuItem item = new JMenuItem(m);
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (project != null) {
          style.nume().change();
          update(project);
        }
      }
    });
    numeration.add(item);
  }
  
  /**
   * Add commands to the menu bar.
   * 
   * @param cmds to be added
   */
  public void addCommands(SCommand... cmds) {
    for (SCommand c : cmds) {
      addCommand(c);
    }
  }
  /**
   * add command to list.
   * @param c command to be added
   */
  private void addCommand(final SCommand c) {
    JMenuItem item = new JMenuItem(c.name());
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (project != null) {
          c.executeOn(project);
          update(project);
        }
      }
    });
    file.add(item);
  }
  
  /**
   * adds styles to frame.
   * @param stls styles
   */
  public void addStyles(SStyle... stls) {
    for (SStyle s : stls) {
      addStyle(s);
    }
  }
  
  /**
   * add a sstyle to frame.
   * @param s sstyle object
   */
  private void addStyle(final SStyle s) {
    JMenuItem estilo = new JMenuItem(s.toString());
    estilo.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (project != null) {
          style = s;
          update(project);
        }
      }
    });
    estilos.add(estilo);
  }
  /**
   * update based on list.
   * @param list to be updated.
   * @param data new data
   */
  public void update(JList list, List data) {
    list.setListData(data.toArray());
    if (data.size() > 0) {
      list.setSelectedIndex(0);
    }
  }

  /**
   * build the entire SFrame.
   */
  private void build() {
    buildPathPanel();
    methods = new JList<SMethod>();
    addOn(methods, BorderLayout.EAST);
    classes = new JList<SClass>();
    addOn(classes, BorderLayout.CENTER);
    packages = new JList<SPackage>();
    addOn(packages, BorderLayout.WEST);
    htmlPanel = buildHTMLPanel(BorderLayout.SOUTH);
    packages.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (packages.getSelectedIndex() != -1) {
          update(classes, packages.getSelectedValue().classes());
        }
      }
    });
    classes.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (classes.getSelectedIndex() != -1) {
          update(methods, classes.getSelectedValue().methods());
        }
      }
    });
    methods.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (methods.getSelectedIndex() != -1) {
          SMethod method = methods.getSelectedValue();
          htmlPanel.setText(method.body().toHTML(style, lighters));
        }
      }
    });

  }
  /**
   * adds the jlist to sframe.
   * @param list to be added
   * @param direction where it should be put
   */
  private void addOn(JList list, String direction) {
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    list.setLayoutOrientation(JList.VERTICAL);
    list.setCellRenderer(new SItemRenderer());
    JScrollPane pane = new JScrollPane(list);
    pane.setMinimumSize(new Dimension(200, 200));
    pane.setPreferredSize(new Dimension(200, 200));
    pane.setMaximumSize(new Dimension(200, 200));
    getContentPane().add(pane, direction);
  }

  /**
   * build the path panel of frame.
   */
  private void buildPathPanel() {
    JPanel pathPanel = new JPanel();
    pathPanel.setLayout(new BorderLayout());
    final JTextField pathField = new JTextField();
    pathField.setEnabled(false);
    JButton browseButton = new JButton("Browse");
    browseButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select a root folder");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(SFrame.this) == JFileChooser.APPROVE_OPTION) {
          pathField.setText(chooser.getSelectedFile().getPath());
          try {
            project = new SFactory().create(chooser.getSelectedFile().getPath());
            update(packages, project.packages());
            numLineas = project.getNumLines();
            numClases = project.getNumClasses();
            numYellows = project.getYellowColor();
            statusbar.add(new JLabel("Lineas: " + numLineas + 
                " Clases: " + numClases + 
                " Yellows : " + numYellows), 
                BorderLayout.CENTER);
            getContentPane().revalidate();
            if (project.packages().size() > 0) {
              packages.setSelectedIndex(0);
            }
          } catch (IOException ex) {
 
          }
        }
      }
    });
    pathPanel.add(pathField, BorderLayout.CENTER);
    pathPanel.add(browseButton, BorderLayout.EAST);
    getContentPane().add(pathPanel, BorderLayout.NORTH);
    
  }

  /**
   * build the html panel.
   * @param direction where it should go
   * @return jeditorpane html
   */
  private JEditorPane buildHTMLPanel(String direction) {
    JPanel Main = new JPanel();
    Main.setLayout(new BorderLayout());
    JEditorPane htmlPanel = new JEditorPane();
    htmlPanel.setEditable(false);
    htmlPanel.setMinimumSize(new Dimension(600, 300));
    htmlPanel.setPreferredSize(new Dimension(600, 300));
    htmlPanel.setMaximumSize(new Dimension(600, 300));
    htmlPanel.setContentType("text/html");
    Main.add(new JScrollPane(htmlPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
        JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), BorderLayout.NORTH);
    Main.add(statusbar, BorderLayout.SOUTH);
    getContentPane().add(Main, direction);
    return htmlPanel;
  }

  /**
   * update project.
   * @param project to be updated
   */
  public void update(SProject project) {
    SPackage pkg = packages.getSelectedValue();
    SClass cls = classes.getSelectedValue();
    SMethod method = methods.getSelectedValue();
    packages.setListData(project.packages().toArray(new SPackage[] {}));
    packages.setSelectedValue(pkg, true);
    if (pkg != null) {
      classes.setListData(pkg.classes().toArray(new SClass[] {}));
      classes.setSelectedValue(cls, true);
      if (cls != null) {
        methods.setListData(cls.methods().toArray(new SMethod[] {}));
        methods.setSelectedValue(method, true);
      }
    }
  }
  /**
   * style to be applied.
   * @param style to apply
   */
  public void style(SStyle style) {
    this.style = style;
    SMethod method = methods.getSelectedValue();
    htmlPanel.setText(method.body().toHTML(style, lighters));
  }
}
